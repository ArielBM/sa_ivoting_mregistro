const mongoose = require('mongoose');
const dbconfig = require('./dbconfig');

var connection = mongoose.connect(`mongodb+srv://${dbconfig.user}:${dbconfig.password}@registercluster.yrtrk.mongodb.net/${dbconfig.database}?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  });

exports.config = function () {
    
    return connection;
}